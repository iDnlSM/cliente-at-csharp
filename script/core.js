var state = {
    btnToggleMenu: document.getElementById("btnToggleMenu"),
    aside: document.getElementById("aside"),
    asideOverlay: document.getElementById("aside-overlay"),
    tbody: document.getElementById("clientes"),
    clientes: new Array(),
    nome: document.getElementById("nome"),
    documento: document.getElementById("documento"),
    sexo: document.getElementById("sexo")
};
window.addEventListener("load", function () {
    var _a, _b;
    (_a = state.btnToggleMenu) === null || _a === void 0 ? void 0 : _a.addEventListener("click", toggleMenu);
    (_b = state.asideOverlay) === null || _b === void 0 ? void 0 : _b.addEventListener("click", toggleMenu);
    getClientes();
});
function toggleMenu(ev) {
    var _a;
    ev.stopPropagation();
    if (((_a = state.aside) === null || _a === void 0 ? void 0 : _a.style.left) === "0px") {
        state.aside.style.left = "-20vw";
        state.asideOverlay.style.zIndex = "0";
    }
    else {
        state.aside.style.left = "0";
    }
}
function populate() {
    var _a;
    (_a = state.tbody) === null || _a === void 0 ? void 0 : _a.replaceChildren();
    for (var _i = 0, _b = state.clientes; _i < _b.length; _i++) {
        var cliente = _b[_i];
        var html = "\n    <tr>\n      <td>".concat(cliente.nome, "</td>\n      <td>").concat(cliente.documento, "</td>\n      <td>").concat(cliente.sexo === 0
            ? "Não informado"
            : cliente.sexo === 1
                ? "Maculino"
                : "Feminino", "</td>\n      <td>\n        <button onclick=\"deleteCliente('").concat(cliente.id, "')\"> Remover </button>\n      </td>\n    </tr>");
        state.tbody.innerHTML += html;
    }
}
