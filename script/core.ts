const state = {
  btnToggleMenu: document.getElementById("btnToggleMenu") as HTMLButtonElement,
  aside: document.getElementById("aside") as HTMLElement,
  asideOverlay: document.getElementById("aside-overlay") as HTMLElement,
  tbody: document.getElementById("clientes") as HTMLElement,

  clientes: new Array<ICliente>(),

  nome: document.getElementById("nome") as HTMLInputElement,
  documento: document.getElementById("documento") as HTMLInputElement,
  sexo: document.getElementById("sexo") as HTMLSelectElement,
};

window.addEventListener("load", () => {
  state.btnToggleMenu?.addEventListener("click", toggleMenu);
  state.asideOverlay?.addEventListener("click", toggleMenu);

  getClientes();
});

function toggleMenu(ev: MouseEvent) {
  ev.stopPropagation();

  if (state.aside?.style.left === "0px") {
    state.aside.style.left = "-20vw";
    state.asideOverlay.style.zIndex = "0";
  } else {
    state.aside.style.left = "0";
  }
}

function populate() {
  state.tbody?.replaceChildren();

  for (const cliente of state.clientes) {
    const html = `
    <tr>
      <td>${cliente.nome}</td>
      <td>${cliente.documento}</td>
      <td>${
        cliente.sexo === 0
          ? "Não informado"
          : cliente.sexo === 1
          ? "Maculino"
          : "Feminino"
      }</td>
      <td>
        <button onclick="deleteCliente('${cliente.id}')"> Remover </button>
      </td>
    </tr>`;

    state.tbody.innerHTML += html;
  }
}
