interface ICliente {
  id?: string;
  nome: string;
  documento: string;
  sexo: number;
}

const url =
  "https://thingproxy.freeboard.io/fetch/https://facec-webapi-2022.herokuapp.com/clientes";

async function getClientes() {
  const response = await fetch(url);

  state.clientes = await response.json();
  populate();
}

async function addCliente() {
  const headers = new Headers();
  headers.set("Content-Type", "application/json");

  const cliente = {
    documento: state.documento.value,
    nome: state.nome.value,
    sexo: Number(state.sexo.value),
  } as ICliente;

  const body = JSON.stringify(cliente);

  await fetch(url, { method: "POST", headers, body });
  getClientes().then();
}

async function deleteCliente(id: string) {
  await fetch(`${url}/${id}`, { method: "DELETE" });
  getClientes().then();
}
